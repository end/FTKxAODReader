#include <utility>
#include <cmath>
#include <algorithm>
#include <string>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <FTKxAODReader/FTKxAODReader.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

#include "TTree.h"

typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;


inline const xAOD::TruthParticle* get_truth(const xAOD::TrackParticle *track)
{
  typedef ElementLink< xAOD::TruthParticleContainer > Link_t;
  static const char* NAME = "truthParticleLink";
  if(!track->isAvailable< Link_t >(NAME)) {
    return NULL;
  }
  const Link_t& link = track->auxdata< Link_t >(NAME);
  if(!link.isValid()) {
    Error("get_truth()", "xAOD::TrackParticle has an invalid truthParticleLink");
    return NULL;
  }
  return *link;
}


std::pair<const xAOD::TrackParticle*, int> best_match(const xAOD::TruthParticle* truth, const xAOD::TrackParticleContainer* tracks)
{
  // Find track that best matches a truth particle
  // Return the best matching track (if any) and index of this track in the
  // container
  int itrack = -1, best_itrack = -1;
  float best_truth_prob = 0., truth_prob;
  const xAOD::TrackParticle* best_track = NULL;
  const xAOD::TruthParticle* matched_truth;
  bool at_least_one_tplink = false;
  for (const auto track : *tracks) {
    ++itrack;
    matched_truth = get_truth(track);
    if (!matched_truth)
      continue;
    at_least_one_tplink = true;
    if (matched_truth->barcode() != truth->barcode()) {
      continue;
    }
    truth_prob = track->auxdata<float>("truthMatchProbability");
    if (truth_prob > best_truth_prob) {
      best_truth_prob = truth_prob;
      best_track = track;
      best_itrack = itrack;
    }
  }
  if (!at_least_one_tplink) {
    Warning("best_match()", "No xAOD::TrackParticles had a truthParticleLink");
  }
  return std::pair<const xAOD::TrackParticle*, int>(best_track, best_itrack);
}


bool FTKxAODReader::isAcceptedTruth(const xAOD::TruthParticle* tp)
{
  // Check to see if it's a stable particle
  if (tp->status() != 1)
    return false;
  // Clearly for tracking we don't care about Neutrals
  if (tp->isNeutral())
    return false;
  // Barcode of zero indicates there was no truth particle found for this track
  if (tp->barcode() == 0 or tp->barcode() >= 200e3)
    return false;
  // Check the particle is within acceptance)
  if (tp->pt() < 1000 || fabs(tp->eta()) > 2.4)
    return false;
  // Make sure particle decays before the last pixel layer
  if (!tp->hasProdVtx() or tp->prodVtx()->perp() > 110)
    return false;
  return true;
}

// this is needed to distribute the algorithm to the workers
ClassImp(FTKxAODReader)


FTKxAODReader::FTKxAODReader(bool truth)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_truth = truth;
  m_simulation = false;
  m_eventCounter = 0;
  m_tsos_offline = true;
  m_tsos_ftk = true;
}


EL::StatusCode FTKxAODReader::setupJob(EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("FTKxAODReader").ignore(); // call before opening first file
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  tree = new TTree("tracks", "tree with track variables per event");
  AddBranches(tree);
  wk()->addOutput(tree);
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::changeInput(bool /*firstfile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  m_event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = NULL;
  if (!m_event->retrieve( eventInfo, "EventInfo").isSuccess()) {
    Error("execute()", "Failed to retrieve event info collection. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  // check if the event is data or simulation
  m_simulation = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // print every 100 events, so we know where we are:
  if((m_eventCounter % 100) == 0) {
    Info("execute()", "Event number = %i", m_eventCounter);
  }
  ++m_eventCounter;

  //----------------------------
  // Event information
  //---------------------------
  const xAOD::EventInfo* eventInfo = NULL;
  if (!m_event->retrieve( eventInfo, "EventInfo").isSuccess()) {
    Error("execute()", "Failed to retrieve event info collection. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  event_number = eventInfo->eventNumber();
  run_number = eventInfo->runNumber();

  const xAOD::TrackParticleContainer* recoTracks = NULL;
  if (!m_event->retrieve(recoTracks, "InDetTrackParticles").isSuccess()) {
    Error("execute()", "Failed to retrieve InDetTrackParticles. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  const xAOD::TrackParticleContainer* ftkTracks = NULL;
  if (!m_event->retrieve(ftkTracks, "FTK_TrackParticleContainer").isSuccess()) {
    Error("execute()", "Failed to retrieve FTK_TrackParticleContainer. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  const xAOD::TrackParticleContainer* ftkRefitTracks = NULL;
  if (!m_event->retrieve(ftkRefitTracks, "FTK_TrackParticleContainerRefit").isSuccess()) {
    Error("execute()", "Failed to retrieve FTK_TrackParticleContainerRefit. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  // Reset branches
  ResetBranches();

  int itruth = -1;
  int ireco = -1;
  int iftk = -1;
  int iftk_refit = -1;

  if (m_truth && m_simulation)
  {
    const xAOD::TruthParticleContainer* truthParticles = NULL;
    if (!m_event->retrieve(truthParticles, "TruthParticles").isSuccess()) {
      Error("execute()", "Failed to retrieve TruthParticles. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    // truth particle loop
    for (auto truthPart: *truthParticles) {
      if (!isAcceptedTruth(truthPart)){
        continue;
      }
      ++itruth;

      truth_track_pt[itruth] = truthPart->pt() / 1000.;
      truth_track_eta[itruth] = truthPart->eta();
      truth_track_charge[itruth] = truthPart->charge();
      truth_track_pdgid[itruth] = truthPart->pdgId();
      truth_track_status[itruth] = truthPart->status();
      truth_track_barcode[itruth] = truthPart->barcode();

      // Tracking decorations for TruthParticles
      /*truth_track_phi[itruth] = truthPart->auxdata<float>("phi");*/
      //truth_track_theta[itruth] = truthPart->auxdata<float>("theta");
      //truth_track_d0[itruth] = truthPart->auxdata<float>("d0");
      //truth_track_z0[itruth] = truthPart->auxdata<float>("z0");
      //truth_track_z0st[itruth] = truthPart->auxdata<float>("z0st");
      /*truth_track_qop[itruth] = truthPart->auxdata<float>("qOverP");*/

      if (truthPart->hasProdVtx())
        truth_track_prod_perp[itruth] = truthPart->prodVtx()->perp();
      else
        truth_track_prod_perp[itruth] = 1e7;

      // Find best offline track match
      Info("FTKxAODReader::execute()", "Looping on InDetTrackParticles to find best truth match");
      auto match = best_match(truthPart, recoTracks);
      if (match.first != NULL) {
        truth_track_reco_match_prob[itruth] = match.first->auxdata<float>("truthMatchProbability");
      } else {
        truth_track_reco_match_prob[itruth] = 0.;
      }
      truth_track_reco_match[itruth] = match.second;

      // Find best FTK track match
      Info("FTKxAODReader::execute()", "Looping on FTK_TrackParticleContainer to find best truth match");
      match = best_match(truthPart, ftkTracks);
      if (match.first != NULL) {
        truth_track_ftk_match_prob[itruth] = match.first->auxdata<float>("truthMatchProbability");
      } else {
        truth_track_ftk_match_prob[itruth] = 0.;
      }
      truth_track_ftk_match[itruth] = match.second;

      // Find best FTK refit track match
      Info("FTKxAODReader::execute()", "Looping on FTK_TrackParticleContainerRefit to find best truth match");
      match = best_match(truthPart, ftkRefitTracks);
      if (match.first != NULL) {
        truth_track_ftk_refit_match_prob[itruth] = match.first->auxdata<float>("truthMatchProbability");
      } else {
        truth_track_ftk_refit_match_prob[itruth] = 0.;
      }
      truth_track_ftk_refit_match[itruth] = match.second;
    }
    truth_track_n = itruth + 1;
  }

  float chi2, ndof;
  int nPix, nSCT;

  // reco track loops
  ireco = -1;
  for (auto track : *recoTracks) {
    ++ireco;

    chi2 = track->chiSquared();
    ndof = track->numberDoF();
    reco_track_pt[ireco] = track->pt();
    reco_track_eta[ireco] = track->eta();
    reco_track_phi[ireco] = track->phi();
    reco_track_theta[ireco] = track->theta();
    reco_track_d0[ireco] = track->auxdata<float>("d0");
    reco_track_z0[ireco] = track->auxdata<float>("z0");
    reco_track_qop[ireco] = track->qOverP();
    reco_track_chi2ndof[ireco] = chi2 / ndof;
    reco_track_charge[ireco] = track->charge();

    //std::cout << std::endl;
    //std::cout << (int)track->auxdata<unsigned char>("numberOfPixelHits") << std::endl;
    //std::cout << (int)track->auxdata<unsigned char>("numberOfSCTHits") << std::endl;

    if (m_tsos_offline) {
      // access msos
      if (!(track)->isAvailable<MeasurementsOnTrack>("msosLink")) {
        Warning("execute()", "Failed to retrieve measurements on offline tracks");
        m_tsos_offline = false;
        continue;
      }

      const MeasurementsOnTrack& measurementsOnTrack = track->auxdata<MeasurementsOnTrack>("msosLink");

      nPix = 0;
      nSCT = 0;

      // measurements of track
      for (MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter) {
        const xAOD::TrackStateValidation* msos = *(*msos_iter);
        //const xAOD::TrackStateValidationContainer* msos = *(*msos_iter);
        //const xAOD::MeasurementStateOnSurface* msos = *(*msos_iter);
        if (!(*msos_iter).isValid()) {
          Warning("execute()", "Found invalid MSOS link on offline track");
          continue;
        }

        //pixel
        if (msos->detType() == 1 and msos->type() == 0) {
          ++nPix;

          if (nPix == 1) {
            reco_track_locx_pix_l1[ireco] = msos->localX();
            reco_track_locy_pix_l1[ireco] = msos->localY();
          }
          else if (nPix == 2) {
            reco_track_locx_pix_l2[ireco] = msos->localX();
            reco_track_locy_pix_l2[ireco] = msos->localY();
          }
          else if (nPix == 3) {
            reco_track_locx_pix_l3[ireco] = msos->localX();
            reco_track_locy_pix_l3[ireco] = msos->localY();
          }
          else if (nPix == 4) {
            reco_track_locx_pix_l4[ireco] = msos->localX();
            reco_track_locy_pix_l4[ireco] = msos->localY();
          }
        }

        //SCT
        else {
          ++nSCT;

          if (nSCT == 1) {
            reco_track_locx_sct_l1[ireco] = msos->localX();
          }
          else if (nSCT == 2) {
            reco_track_locx_sct_l2[ireco] = msos->localX();
          }
          else if (nSCT == 3) {
            reco_track_locx_sct_l3[ireco] = msos->localX();
          }
          else if (nSCT == 4) {
            reco_track_locx_sct_l4[ireco] = msos->localX();
          }
          else if (nSCT == 5) {
            reco_track_locx_sct_l5[ireco] = msos->localX();
          }
        }
      }

      reco_track_npixhit[ireco] = nPix;
      reco_track_nscthit[ireco] = nSCT;
    }
  }
  reco_track_n = ireco + 1;

  // ftk track loops
  iftk = -1;
  for (auto track : *ftkTracks) {
    ++iftk;

    chi2 = track->chiSquared();
    ndof = track->numberDoF();
    ftk_track_pt[iftk] = track->pt();
    ftk_track_eta[iftk] = track->eta();
    ftk_track_phi[iftk] = track->phi();
    ftk_track_theta[iftk] = track->theta();
    ftk_track_d0[iftk] = track->auxdata<float>("d0");
    ftk_track_z0[iftk] = track->auxdata<float>("z0");
    ftk_track_qop[iftk] = track->qOverP();
    ftk_track_chi2ndof[iftk] = chi2 / ndof;
    ftk_track_charge[iftk] = track->charge();

    if (m_tsos_ftk) {
      // access msos
      if(!(track)->isAvailable<MeasurementsOnTrack>("msosLink") ) {
        Warning("execute()", "Failed to retrieve measurements on ftk tracks");
        m_tsos_ftk = false;
        continue;
      }

      const MeasurementsOnTrack& measurementsOnTrack = track->auxdata<MeasurementsOnTrack>("msosLink");

      nPix = 0;
      nSCT = 0;

      // measurements of track
      for( MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter){
        const xAOD::TrackStateValidation* msos = *(*msos_iter);
        //const xAOD::TrackStateValidationContainer* msos = *(*msos_iter);
        //const xAOD::MeasurementStateOnSurface* msos = *(*msos_iter);
        if( ! (*msos_iter).isValid() ){
          Warning("execute()", "Found invalid MSOS link on FTK track");
          continue;
        }
        //pixel
        if( msos->detType() == 1  and  msos->type() ==0 ) {
          ++nPix;

          if (nPix ==1){
            ftk_track_locx_pix_l1[iftk] = msos->localX();
            ftk_track_locy_pix_l1[iftk] = msos->localY();
          }
          else if (nPix ==2){
            ftk_track_locx_pix_l2[iftk] = msos->localX();
            ftk_track_locy_pix_l2[iftk] = msos->localY();
          }
          else if (nPix ==3){
            ftk_track_locx_pix_l3[iftk] = msos->localX();
            ftk_track_locy_pix_l3[iftk] = msos->localY();
          }
          else if (nPix ==4){
            ftk_track_locx_pix_l4[iftk] = msos->localX();
            ftk_track_locy_pix_l4[iftk] = msos->localY();
          }
        }

        //SCT
        else{
          ++nSCT;

          if (nSCT ==1){
            ftk_track_locx_sct_l1[iftk] = msos->localX();
          }
          else if (nSCT ==2){
            ftk_track_locx_sct_l2[iftk] = msos->localX();
          }
          else if (nSCT ==3){
            ftk_track_locx_sct_l3[iftk] = msos->localX();
          }
          else if (nSCT ==4){
            ftk_track_locx_sct_l4[iftk] = msos->localX();
          }
          else if (nSCT ==5){
            ftk_track_locx_sct_l5[iftk] = msos->localX();
          }
        }
      }

      ftk_track_npixhit[iftk] = nPix;
      ftk_track_nscthit[iftk] = nSCT;
    }
  }
  ftk_track_n = iftk + 1;

  // ftk refit track loops
  iftk_refit = -1;
  for (auto track : *ftkRefitTracks) {
    ++iftk_refit;

    chi2 = track->chiSquared();
    ndof = track->numberDoF();
    ftk_refit_track_pt[iftk_refit] = track->pt();
    ftk_refit_track_eta[iftk_refit] = track->eta();
    ftk_refit_track_phi[iftk_refit] = track->phi();
    ftk_refit_track_theta[iftk_refit] = track->theta();
    ftk_refit_track_d0[iftk_refit] = track->auxdata<float>("d0");
    ftk_refit_track_z0[iftk_refit] = track->auxdata<float>("z0");
    ftk_refit_track_qop[iftk_refit] = track->qOverP();
    ftk_refit_track_chi2ndof[iftk_refit] = chi2 / ndof;
    ftk_refit_track_charge[iftk_refit] = track->charge();

    if (m_tsos_ftk) {
      // access msos
      if(!(track)->isAvailable< MeasurementsOnTrack >( "msosLink" ) ) {
        Warning("execute()", "Failed to retrieve measurements on ftk refit tracks");
        m_tsos_ftk = false;
        continue;
      }

      const MeasurementsOnTrack& measurementsOnTrack = track->auxdata<MeasurementsOnTrack>("msosLink");

      nPix = 0;
      nSCT = 0;

      // measurements of track
      for( MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter){
        const xAOD::TrackStateValidation* msos = *(*msos_iter);
        //const xAOD::TrackStateValidationContainer* msos = *(*msos_iter);
        //const xAOD::MeasurementStateOnSurface* msos = *(*msos_iter);
        if( ! (*msos_iter).isValid() ){
          Warning("execute()", "Found invalid MSOS link on refit FTK track");
          continue;
        }
        //pixel
        if( msos->detType() == 1  and  msos->type() == 0 ) {
          ++nPix;

          if (nPix ==1){
            ftk_refit_track_locx_pix_l1[iftk_refit] = msos->localX();
            ftk_refit_track_locy_pix_l1[iftk_refit] = msos->localY();
          }
          else if (nPix ==2){
            ftk_refit_track_locx_pix_l2[iftk_refit] = msos->localX();
            ftk_refit_track_locy_pix_l2[iftk_refit] = msos->localY();
          }
          else if (nPix ==3){
            ftk_refit_track_locx_pix_l3[iftk_refit] = msos->localX();
            ftk_refit_track_locy_pix_l3[iftk_refit] = msos->localY();
          }
          else if (nPix ==4){
            ftk_refit_track_locx_pix_l4[iftk_refit] = msos->localX();
            ftk_refit_track_locy_pix_l4[iftk_refit] = msos->localY();
          }
        }

        //SCT
        else{
          ++nSCT;

          if (nSCT ==1){
            ftk_refit_track_locx_sct_l1[iftk_refit] = msos->localX();
          }
          else if (nSCT ==2){
            ftk_refit_track_locx_sct_l2[iftk_refit] = msos->localX();
          }
          else if (nSCT ==3){
            ftk_refit_track_locx_sct_l3[iftk_refit] = msos->localX();
          }
          else if (nSCT ==4){
            ftk_refit_track_locx_sct_l4[iftk_refit] = msos->localX();
          }
          else if (nSCT ==5){
            ftk_refit_track_locx_sct_l5[iftk_refit] = msos->localX();
          }
        }
      }

      ftk_refit_track_npixhit[iftk_refit] = nPix;
      ftk_refit_track_nscthit[iftk_refit] = nSCT;
    }
  }
  ftk_refit_track_n = iftk_refit + 1;

  // fill the tree
  tree->Fill();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode FTKxAODReader::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}


bool FTKxAODReader::passTrackQualitySelection(const xAOD::TrackParticle* track)
{
  uint8_t numberOfSCTHits= 0;
  uint8_t expectBLayerHit= 0;
  uint8_t numberOfBLayerHits= 0;
  uint8_t numberOfPixelHits= 0;
  if(!track->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits))
    Error("passTrackQualitySelection()", "Failed to retrieved summaryValue Pixel Hits" );
  if(!track->summaryValue(numberOfSCTHits,xAOD::numberOfSCTHits))
    Error("passTrackQualitySelection()", "Failed to retrieved summaryValue SCT hits" );
  if(!track->summaryValue(numberOfBLayerHits,xAOD::numberOfBLayerHits))
    Error("passTrackQualitySelection()", "Failed to retrieved summaryValue Blayer hits" );
  if(!track->summaryValue(expectBLayerHit,xAOD::expectBLayerHit))
    Error("passTrackQualitySelection()", "Failed to retrieved summaryValue expected blayer" );
  if(expectBLayerHit >= 1 && numberOfBLayerHits==0)
    return false;
  if(numberOfPixelHits < 1)
    return false;
  if(numberOfSCTHits < 6)
    return false;
  if(track->pt() > 10e3
      && TMath::Prob( track->chiSquared(), track->numberDoF() ) <= 0.01 )
    return false;
  return true;
}


bool FTKxAODReader::passIP_Selection(const xAOD::TrackParticle* track, const xAOD::Vertex* vertex)
{
  if(!vertex)
    return false;
  if(std::fabs(track->d0()) >= 2.0)
    return false;
  if(std::fabs(track->z0() + track->vz() - vertex->z() )
      * TMath::Sin(track->theta()) >= 1.5 )
    return false;
  return true;
}


void FTKxAODReader::AddBranches(TTree* tree) {
  tree->Branch("event_number", &event_number, "event_number/I");
  tree->Branch("run_number",   &run_number,   "run_number/I");

  tree->Branch("reco_track_n",      &reco_track_n,      "reco_track_n/I");
  tree->Branch("ftk_track_n",       &ftk_track_n,       "ftk_track_n/I");
  tree->Branch("ftk_refit_track_n", &ftk_refit_track_n, "ftk_refit_track_n/I");

  if (m_truth && m_simulation)
  {
    tree->Branch("truth_track_n",     &truth_track_n,     "truth_track_n/I");
    tree->Branch("truth_track_reco_match",      &truth_track_reco_match,      "truth_track_reco_match[truth_track_n]/I");
    tree->Branch("truth_track_ftk_match",       &truth_track_ftk_match,       "truth_track_ftk_match[truth_track_n]/I");
    tree->Branch("truth_track_ftk_refit_match", &truth_track_ftk_refit_match, "truth_track_ftk_refit_match[truth_track_n]/I");

    tree->Branch("truth_track_reco_match_prob",      &truth_track_reco_match_prob,      "truth_track_reco_match_prob[truth_track_n]/F");
    tree->Branch("truth_track_ftk_match_prob",       &truth_track_ftk_match_prob,       "truth_track_ftk_match_prob[truth_track_n]/F");
    tree->Branch("truth_track_ftk_refit_match_prob", &truth_track_ftk_refit_match_prob, "truth_track_ftk_refit_match_prob[truth_track_n]/F");

    tree->Branch("truth_track_pt",        &truth_track_pt,        "truth_track_pt[truth_track_n]/F");
    tree->Branch("truth_track_eta",       &truth_track_eta,       "truth_track_eta[truth_track_n]/F");
    tree->Branch("truth_track_phi",       &truth_track_phi,       "truth_track_phi[truth_track_n]/F");
    tree->Branch("truth_track_theta",     &truth_track_theta,     "truth_track_theta[truth_track_n]/F");
    tree->Branch("truth_track_d0",        &truth_track_d0,        "truth_track_d0[truth_track_n]/F");
    tree->Branch("truth_track_z0",        &truth_track_z0,        "truth_track_z0[truth_track_n]/F");
    tree->Branch("truth_track_z0st",      &truth_track_z0st,      "truth_track_z0st[truth_track_n]/F");
    tree->Branch("truth_track_qop",       &truth_track_qop,       "truth_track_qop[truth_track_n]/F");
    tree->Branch("truth_track_charge",    &truth_track_charge,    "truth_track_charge[truth_track_n]/I");
    tree->Branch("truth_track_pdgid",     &truth_track_pdgid,     "truth_track_pdgid[truth_track_n]/I");
    tree->Branch("truth_track_status",    &truth_track_status,    "truth_track_status[truth_track_n]/I");
    tree->Branch("truth_track_prod_perp", &truth_track_prod_perp, "truth_track_prod_perp[truth_track_n]/F");
    tree->Branch("truth_track_barcode",   &truth_track_barcode,   "truth_track_barcode[truth_track_n]/I");
  }

  tree->Branch("reco_track_pt",       &reco_track_pt,       "reco_track_pt[reco_track_n]/F");
  tree->Branch("reco_track_eta",      &reco_track_eta,      "reco_track_eta[reco_track_n]/F");
  tree->Branch("reco_track_phi",      &reco_track_phi,      "reco_track_phi[reco_track_n]/F");
  tree->Branch("reco_track_theta",    &reco_track_theta,    "reco_track_theta[reco_track_n]/F");
  tree->Branch("reco_track_d0",       &reco_track_d0,       "reco_track_d0[reco_track_n]/F");
  tree->Branch("reco_track_z0",       &reco_track_z0,       "reco_track_z0[reco_track_n]/F");
  tree->Branch("reco_track_qop",      &reco_track_qop,      "reco_track_qop[reco_track_n]/F");
  tree->Branch("reco_track_charge",   &reco_track_charge,   "reco_track_charge[reco_track_n]/I");
  tree->Branch("reco_track_chi2ndof", &reco_track_chi2ndof, "reco_track_chi2ndof[reco_track_n]/F");
  tree->Branch("reco_track_npixhit",  &reco_track_npixhit,  "reco_track_npixhit[reco_track_n]/I");
  tree->Branch("reco_track_nscthit",  &reco_track_nscthit,  "reco_track_nscthit[reco_track_n]/I");

  tree->Branch("ftk_track_pt",       &ftk_track_pt,       "ftk_track_pt[ftk_track_n]/F");
  tree->Branch("ftk_track_eta",      &ftk_track_eta,      "ftk_track_eta[ftk_track_n]/F");
  tree->Branch("ftk_track_phi",      &ftk_track_phi,      "ftk_track_phi[ftk_track_n]/F");
  tree->Branch("ftk_track_theta",    &ftk_track_theta,    "ftk_track_theta[ftk_track_n]/F");
  tree->Branch("ftk_track_d0",       &ftk_track_d0,       "ftk_track_d0[ftk_track_n]/F");
  tree->Branch("ftk_track_z0",       &ftk_track_z0,       "ftk_track_z0[ftk_track_n]/F");
  tree->Branch("ftk_track_qop",      &ftk_track_qop,      "ftk_track_qop[ftk_track_n]/F");
  tree->Branch("ftk_track_charge",   &ftk_track_charge,   "ftk_track_charge[ftk_track_n]/I");
  tree->Branch("ftk_track_chi2ndof", &ftk_track_chi2ndof, "ftk_track_chi2ndof[ftk_track_n]/F");
  tree->Branch("ftk_track_npixhit",  &ftk_track_npixhit,  "ftk_track_npixhit[ftk_track_n]/I");
  tree->Branch("ftk_track_nscthit",  &ftk_track_nscthit,  "ftk_track_nscthit[ftk_track_n]/I");

  tree->Branch("ftk_refit_track_pt",       &ftk_refit_track_pt,       "ftk_refit_track_pt[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_eta",      &ftk_refit_track_eta,      "ftk_refit_track_eta[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_phi",      &ftk_refit_track_phi,      "ftk_refit_track_phi[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_theta",    &ftk_refit_track_theta,    "ftk_refit_track_theta[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_d0",       &ftk_refit_track_d0,       "ftk_refit_track_d0[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_z0",       &ftk_refit_track_z0,       "ftk_refit_track_z0[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_qop",      &ftk_refit_track_qop,      "ftk_refit_track_qop[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_charge",   &ftk_refit_track_charge,   "ftk_refit_track_charge[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_chi2ndof", &ftk_refit_track_chi2ndof, "ftk_refit_track_chi2ndof[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_npixhit",  &ftk_refit_track_npixhit,  "ftk_refit_track_npixhit[ftk_refit_track_n]/I");
  tree->Branch("ftk_refit_track_nscthit",  &ftk_refit_track_nscthit,  "ftk_refit_track_nscthit[ftk_refit_track_n]/I");

  // MSOS
  tree->Branch("reco_track_locx_pix_l1", &reco_track_locx_pix_l1, "reco_track_locx_pix_l1[reco_track_n]/F");
  tree->Branch("reco_track_locy_pix_l1", &reco_track_locy_pix_l1, "reco_track_locy_pix_l1[reco_track_n]/F");
  tree->Branch("reco_track_locx_pix_l2", &reco_track_locx_pix_l2, "reco_track_locx_pix_l2[reco_track_n]/F");
  tree->Branch("reco_track_locy_pix_l2", &reco_track_locy_pix_l2, "reco_track_locy_pix_l2[reco_track_n]/F");
  tree->Branch("reco_track_locx_pix_l3", &reco_track_locx_pix_l3, "reco_track_locx_pix_l3[reco_track_n]/F");
  tree->Branch("reco_track_locy_pix_l3", &reco_track_locy_pix_l3, "reco_track_locy_pix_l3[reco_track_n]/F");
  tree->Branch("reco_track_locx_pix_l4", &reco_track_locx_pix_l4, "reco_track_locx_pix_l4[reco_track_n]/F");
  tree->Branch("reco_track_locy_pix_l4", &reco_track_locy_pix_l4, "reco_track_locy_pix_l4[reco_track_n]/F");

  tree->Branch("reco_track_locx_sct_l1", &reco_track_locx_sct_l1, "reco_track_locx_sct_l1[reco_track_n]/F");
  tree->Branch("reco_track_locx_sct_l2", &reco_track_locx_sct_l2, "reco_track_locx_sct_l2[reco_track_n]/F");
  tree->Branch("reco_track_locx_sct_l3", &reco_track_locx_sct_l3, "reco_track_locx_sct_l3[reco_track_n]/F");
  tree->Branch("reco_track_locx_sct_l4", &reco_track_locx_sct_l4, "reco_track_locx_sct_l4[reco_track_n]/F");
  tree->Branch("reco_track_locx_sct_l5", &reco_track_locx_sct_l5, "reco_track_locx_sct_l5[reco_track_n]/F");

  tree->Branch("ftk_track_locx_pix_l1", &ftk_track_locx_pix_l1, "ftk_track_locx_pix_l1[ftk_track_n]/F");
  tree->Branch("ftk_track_locy_pix_l1", &ftk_track_locy_pix_l1, "ftk_track_locy_pix_l1[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_pix_l2", &ftk_track_locx_pix_l2, "ftk_track_locx_pix_l2[ftk_track_n]/F");
  tree->Branch("ftk_track_locy_pix_l2", &ftk_track_locy_pix_l2, "ftk_track_locy_pix_l2[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_pix_l3", &ftk_track_locx_pix_l3, "ftk_track_locx_pix_l3[ftk_track_n]/F");
  tree->Branch("ftk_track_locy_pix_l3", &ftk_track_locy_pix_l3, "ftk_track_locy_pix_l3[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_pix_l4", &ftk_track_locx_pix_l4, "ftk_track_locx_pix_l4[ftk_track_n]/F");
  tree->Branch("ftk_track_locy_pix_l4", &ftk_track_locy_pix_l4, "ftk_track_locy_pix_l4[ftk_track_n]/F");

  tree->Branch("ftk_track_locx_sct_l1", &ftk_track_locx_sct_l1, "ftk_track_locx_sct_l1[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_sct_l2", &ftk_track_locx_sct_l2, "ftk_track_locx_sct_l2[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_sct_l3", &ftk_track_locx_sct_l3, "ftk_track_locx_sct_l3[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_sct_l4", &ftk_track_locx_sct_l4, "ftk_track_locx_sct_l4[ftk_track_n]/F");
  tree->Branch("ftk_track_locx_sct_l5", &ftk_track_locx_sct_l5, "ftk_track_locx_sct_l5[ftk_track_n]/F");

  tree->Branch("ftk_refit_track_locx_pix_l1",  &ftk_refit_track_locx_pix_l1,  "ftk_refit_track_locx_pix_l1[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locy_pix_l1",  &ftk_refit_track_locy_pix_l1,  "ftk_refit_track_locy_pix_l1[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_pix_l2",  &ftk_refit_track_locx_pix_l2,  "ftk_refit_track_locx_pix_l2[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locy_pix_l2",  &ftk_refit_track_locy_pix_l2,  "ftk_refit_track_locy_pix_l2[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_pix_l3",  &ftk_refit_track_locx_pix_l3,  "ftk_refit_track_locx_pix_l3[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locy_pix_l3",  &ftk_refit_track_locy_pix_l3,  "ftk_refit_track_locy_pix_l3[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_pix_l4",  &ftk_refit_track_locx_pix_l4,  "ftk_refit_track_locx_pix_l4[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locy_pix_l4",  &ftk_refit_track_locy_pix_l4,  "ftk_refit_track_locy_pix_l4[ftk_refit_track_n]/F");

  tree->Branch("ftk_refit_track_locx_sct_l1",  &ftk_refit_track_locx_sct_l1,  "ftk_refit_track_locx_sct_l1[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_sct_l2",  &ftk_refit_track_locx_sct_l2,  "ftk_refit_track_locx_sct_l2[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_sct_l3",  &ftk_refit_track_locx_sct_l3,  "ftk_refit_track_locx_sct_l3[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_sct_l4",  &ftk_refit_track_locx_sct_l4,  "ftk_refit_track_locx_sct_l4[ftk_refit_track_n]/F");
  tree->Branch("ftk_refit_track_locx_sct_l5",  &ftk_refit_track_locx_sct_l5,  "ftk_refit_track_locx_sct_l5[ftk_refit_track_n]/F");
}

void FTKxAODReader::ResetBranches() {
  // Reset MSOS info since they might not be set for all layers on each track
  std::fill_n(reco_track_locx_pix_l1, 5000, NAN);
  std::fill_n(reco_track_locy_pix_l1, 5000, NAN);
  std::fill_n(reco_track_locx_pix_l2, 5000, NAN);
  std::fill_n(reco_track_locy_pix_l2, 5000, NAN);
  std::fill_n(reco_track_locx_pix_l3, 5000, NAN);
  std::fill_n(reco_track_locy_pix_l3, 5000, NAN);
  std::fill_n(reco_track_locx_pix_l4, 5000, NAN);
  std::fill_n(reco_track_locy_pix_l4, 5000, NAN);

  std::fill_n(reco_track_locx_sct_l1, 5000, NAN);
  std::fill_n(reco_track_locx_sct_l2, 5000, NAN);
  std::fill_n(reco_track_locx_sct_l3, 5000, NAN);
  std::fill_n(reco_track_locx_sct_l4, 5000, NAN);
  std::fill_n(reco_track_locx_sct_l5, 5000, NAN);

  std::fill_n(ftk_track_locx_pix_l1, 5000, NAN);
  std::fill_n(ftk_track_locy_pix_l1, 5000, NAN);
  std::fill_n(ftk_track_locx_pix_l2, 5000, NAN);
  std::fill_n(ftk_track_locy_pix_l2, 5000, NAN);
  std::fill_n(ftk_track_locx_pix_l3, 5000, NAN);
  std::fill_n(ftk_track_locy_pix_l3, 5000, NAN);
  std::fill_n(ftk_track_locx_pix_l4, 5000, NAN);
  std::fill_n(ftk_track_locy_pix_l4, 5000, NAN);

  std::fill_n(ftk_track_locx_sct_l1, 5000, NAN);
  std::fill_n(ftk_track_locx_sct_l2, 5000, NAN);
  std::fill_n(ftk_track_locx_sct_l3, 5000, NAN);
  std::fill_n(ftk_track_locx_sct_l4, 5000, NAN);
  std::fill_n(ftk_track_locx_sct_l5, 5000, NAN);

  std::fill_n(ftk_refit_track_locx_pix_l1, 5000, NAN);
  std::fill_n(ftk_refit_track_locy_pix_l1, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_pix_l2, 5000, NAN);
  std::fill_n(ftk_refit_track_locy_pix_l2, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_pix_l3, 5000, NAN);
  std::fill_n(ftk_refit_track_locy_pix_l3, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_pix_l4, 5000, NAN);
  std::fill_n(ftk_refit_track_locy_pix_l4, 5000, NAN);

  std::fill_n(ftk_refit_track_locx_sct_l1, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_sct_l2, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_sct_l3, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_sct_l4, 5000, NAN);
  std::fill_n(ftk_refit_track_locx_sct_l5, 5000, NAN);
}
