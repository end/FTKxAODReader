from AthenaCommon.AppMgr import ToolSvc, ServiceMgr
from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.GlobalFlags import globalflags

IsSimulation = (globalflags.DataSource == 'geant4' and
                globalflags.InputFormat == 'pool')

DAOD_doPix = True
DAOD_doSct = True
DAOD_doTrt = False

from TriggerJobOpts.TriggerFlags import TriggerFlags

TriggerFlags.doFTK = True

from RecExConfig.RecFlags import rec

rec.doAOD = True
rec.doESD = True
rec.doWriteAOD = True
rec.doWriteESD = True
rec.doWriteTAG = False
rec.doTagRawSummary = False
rec.doTrigger = False
rec.doCalo = False
rec.doInDet = True
rec.doMuon = False
rec.doMuonCombined = False
rec.doJetMissingETTag = False
rec.doEgamma = False
rec.doTau = False
rec.doLucid = False
rec.doTruth.set_Value_and_Lock(IsSimulation)
rec.doFTK.set_Value_and_Lock(True)

from AthenaCommon.DetFlags import DetFlags

DetFlags.all_setOn()

from InDetRecExample.InDetJobProperties import InDetFlags

InDetFlags.doTruth.set_Value_and_Lock(IsSimulation)
InDetFlags.doxAOD.set_Value_and_Lock(True)
InDetFlags.loadTools.set_Value_and_Lock(True)

include('RecExCommon/RecExCommon_topOptions.py')

topSequence = AlgSequence()

# Temporary fix not needed from 21.0.20 onwards where "Trig" has been removed
# from all collection names
ServiceMgr.TrigFTK_DataProviderSvc.TrackCollectionName = "FTK_TrackCollection"
ServiceMgr.TrigFTK_DataProviderSvc.TrackParticleContainerName = "FTK_TrackParticleContainer"
ServiceMgr.TrigFTK_DataProviderSvc.VertexContainerName = "FTK_VertexContainer"
ServiceMgr.TrigFTK_DataProviderSvc.PixelTruthName = "FTK_PRD_MultiTruthPixel"
ServiceMgr.TrigFTK_DataProviderSvc.SctTruthName = "FTK_PRD_MultiTruthSCT"
ServiceMgr.TrigFTK_DataProviderSvc.PixelClusterContainerName = "FTK_PixelClusterContainer"
ServiceMgr.TrigFTK_DataProviderSvc.SCT_ClusterContainerName = "FTK_SCT_ClusterContainer"
topSequence.FTK_Tracks_DetailedTruthCollectionMaker.TrackCollectionName = "FTK_TrackCollection"
topSequence.FTK_RefitTracks_DetailedTruthCollectionMaker.TrackCollectionName = "FTK_TrackCollectionRefit"
#topSequence.FTK_Tracks_DetailedTruthCollectionMaker.TruthNamePixel = "TrigFTK_PRD_MultiTruthPixel"
#topSequence.FTK_Tracks_DetailedTruthCollectionMaker.TruthNameSCT = "TrigFTK_PRD_MultiTruthSCT"
#topSequence.FTK_RefitTracks_DetailedTruthCollectionMaker.TruthNamePixel = "TrigFTK_PRD_MultiTruthPixel"
#topSequence.FTK_RefitTracks_DetailedTruthCollectionMaker.TruthNameSCT = "TrigFTK_PRD_MultiTruthSCT"

if DAOD_doTrt:
    from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_StrawNeighbourSvc
    TRTStrawNeighbourSvc = TRT_StrawNeighbourSvc()
    ServiceMgr += TRTStrawNeighbourSvc
    from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc
    TRTCalibDBSvc = TRT_CalDbSvc()
    ServiceMgr += TRTCalibDBSvc

    from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import TRT_PrepDataToxAOD
    xAOD_TRT_PrepDataToxAOD = TRT_PrepDataToxAOD(name="xAOD_TRT_PrepDataToxAOD")
    xAOD_TRT_PrepDataToxAOD.OutputLevel = INFO
    xAOD_TRT_PrepDataToxAOD.UseTruthInfo = IsSimulation
    topSequence += xAOD_TRT_PrepDataToxAOD

if DAOD_doSct:
    from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import SCT_PrepDataToxAOD
    xAOD_SCT_PrepDataToxAOD = SCT_PrepDataToxAOD(name="xAOD_SCT_PrepDataToxAOD")
    xAOD_SCT_PrepDataToxAOD.OutputLevel = INFO
    xAOD_SCT_PrepDataToxAOD.UseTruthInfo = IsSimulation
    topSequence += xAOD_SCT_PrepDataToxAOD

if DAOD_doPix:
    from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import PixelPrepDataToxAOD
    xAOD_PixelPrepDataToxAOD = PixelPrepDataToxAOD(name="xAOD_PixelPrepDataToxAOD")
    xAOD_PixelPrepDataToxAOD.OutputLevel = INFO
    xAOD_PixelPrepDataToxAOD.UseTruthInfo = IsSimulation
    topSequence += xAOD_PixelPrepDataToxAOD

# Begin derivation configuration
augmentation_tools = []

# Add track-to-vertex decorations
from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import (
    Trk__TrackToVertexIPEstimator)

IPETool = Trk__TrackToVertexIPEstimator(name="IPETool")
ToolSvc += IPETool

from DerivationFrameworkInDet.DerivationFrameworkInDetConf import (
    DerivationFramework__TrackToVertexWrapper)

TrackToVertexWrapper= DerivationFramework__TrackToVertexWrapper(
    name="TrackToVertexWrapper",
    TrackToVertexIPEstimator=IPETool,
    DecorationPrefix="ttv",
    ContainerName="InDetTrackParticles")
ToolSvc += TrackToVertexWrapper
augmentation_tools.append(TrackToVertexWrapper)

FTKTrackToVertexWrapper= DerivationFramework__TrackToVertexWrapper(
    name="FTKTrackToVertexWrapper",
    TrackToVertexIPEstimator=IPETool,
    DecorationPrefix="ttv",
    ContainerName="FTK_TrackParticleContainer")
ToolSvc += FTKTrackToVertexWrapper
augmentation_tools.append(FTKTrackToVertexWrapper)

FTKRefitTrackToVertexWrapper= DerivationFramework__TrackToVertexWrapper(
    name="RefitFTKTrackToVertexWrapper",
    TrackToVertexIPEstimator=IPETool,
    DecorationPrefix="ttv",
    ContainerName="FTK_TrackParticleContainerRefit")
ToolSvc += FTKRefitTrackToVertexWrapper
augmentation_tools.append(FTKRefitTrackToVertexWrapper)

# Add track parameter decoration to TruthParticles if running on simulation
if IsSimulation:
    from DerivationFrameworkInDet.DerivationFrameworkInDetConf import (
        DerivationFramework__TrackParametersForTruthParticles)

    TruthDecor = DerivationFramework__TrackParametersForTruthParticles(
        name="TruthTPDecor",
        TruthParticleContainerName="TruthParticles",
        DecorationPrefix="")
    ToolSvc += TruthDecor
    augmentation_tools.append(TruthDecor)

# Add the TSOS decoration
# This requires
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import (
    DerivationFramework__TrackStateOnSurfaceDecorator)

TSOS = DerivationFramework__TrackStateOnSurfaceDecorator(
    name="TrackStateOnSurfaceDecorator",
    ContainerName="InDetTrackParticles",
    IsSimulation=False,
    DecorationPrefix="",
    StoreTRT=DAOD_doTrt,
    StoreSCT=DAOD_doSct,
    StorePixel=DAOD_doPix,
    OutputLevel=INFO)
ToolSvc += TSOS
augmentation_tools.append(TSOS)

FTK_TSOS = DerivationFramework__TrackStateOnSurfaceDecorator(
    name="FTK_TrackStateOnSurfaceDecorator",
    ContainerName="FTK_TrackParticleContainer",
    DecorationPrefix="",
    StoreTRT=DAOD_doTrt,
    StoreSCT=DAOD_doSct,
    StorePixel=DAOD_doPix,
    PixelMsosName="FTK_PixelMSOSs",
    SctMsosName="FTK_SCT_MSOSs",
    TrtMsosName="FTK_TRT_MSOSs",
    OutputLevel=INFO)
ToolSvc += FTK_TSOS
augmentation_tools.append(FTK_TSOS)

FTK_Refit_TSOS = DerivationFramework__TrackStateOnSurfaceDecorator(
    name="RefitFTK_TrackStateOnSurfaceDecorator",
    ContainerName="FTK_TrackParticleContainerRefit",
    DecorationPrefix="",
    StoreTRT=DAOD_doTrt,
    StoreSCT=DAOD_doSct,
    StorePixel=DAOD_doPix,
    PixelMsosName="RefitFTK_PixelMSOSs",
    SctMsosName="RefitFTK_SCT_MSOSs",
    TrtMsosName="RefitFTK_TRT_MSOSs",
    OutputLevel=INFO)
ToolSvc += FTK_Refit_TSOS
augmentation_tools.append(FTK_Refit_TSOS)

# Set up derivation framework
from AthenaCommon import CfgMgr

DerivationFrameworkJob = CfgMgr.AthSequencer("FTKSeq")

# Set up stream auditor
from AthenaCommon.AppMgr import ServiceMgr

if not hasattr(ServiceMgr, 'DecisionSvc'):
    ServiceMgr += CfgMgr.DecisionSvc()
ServiceMgr.DecisionSvc.CalcStats = True

from DerivationFrameworkCore.DerivationFrameworkCoreConf import (
    DerivationFramework__CommonAugmentation)

DerivationFrameworkJob += CfgMgr.DerivationFramework__CommonAugmentation(
    "DAOD_FTK_Kernel",
    AugmentationTools=augmentation_tools,
    OutputLevel=INFO)
topSequence += DerivationFrameworkJob

# Create DAOD output stream
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr

DAODStream = MSMgr.NewPoolRootStream("DAOD_FTK", "DAOD_FTK.pool.root")
DAODStream.AddItem("xAOD::EventInfo#*")
DAODStream.AddItem("xAOD::EventAuxInfo#*")

if IsSimulation:
    # Truth containers
    DAODStream.AddItem("xAOD::TruthEventContainer#*")
    DAODStream.AddItem("xAOD::TruthEventAuxContainer#*")

    DAODStream.AddItem("xAOD::TruthParticleContainer#TruthParticles")
    DAODStream.AddItem("xAOD::TruthParticleAuxContainer#TruthParticlesAux.")

    DAODStream.AddItem("xAOD::TruthVertexContainer#TruthVertices")
    DAODStream.AddItem("xAOD::TruthVertexAuxContainer#TruthVerticesAux.")

# Offline containers
DAODStream.AddItem("xAOD::TrackParticleContainer#InDetTrackParticles")
DAODStream.AddItem("xAOD::TrackParticleAuxContainer#InDetTrackParticlesAux.-caloExtension")

DAODStream.AddItem("xAOD::VertexContainer#PrimaryVertices")
DAODStream.AddItem("xAOD::VertexAuxContainer#PrimaryVerticesAux.-vxTrackAtVertex")

# FTK containers
DAODStream.AddItem("xAOD::TrackParticleContainer#FTK_TrackParticleContainer")
DAODStream.AddItem("xAOD::TrackParticleAuxContainer#FTK_TrackParticleContainerAux.")

DAODStream.AddItem("xAOD::TrackParticleContainer#FTK_TrackParticleContainerRefit")
DAODStream.AddItem("xAOD::TrackParticleAuxContainer#FTK_TrackParticleContainerRefitAux.")

DAODStream.AddItem("xAOD::VertexContainer#FTK_VertexContainer")
DAODStream.AddItem("xAOD::VertexAuxContainer#FTK_VertexContainerAux.-vxTrackAtVertex")

DAODStream.AddItem("xAOD::VertexContainer#FTK_VertexContainerRefit")
DAODStream.AddItem("xAOD::VertexAuxContainer#FTK_VertexContainerRefitAux.-vxTrackAtVertex")

# Extra track information
DAODStream.AddItem("xAOD::TrackStateValidationContainer#*")
DAODStream.AddItem("xAOD::TrackStateValidationAuxContainer#*")

DAODStream.AddItem("xAOD::TrackMeasurementValidationContainer#*")
DAODStream.AddItem("xAOD::TrackMeasurementValidationAuxContainer#*")
