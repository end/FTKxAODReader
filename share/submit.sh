#!/bin/bash

# See list of datasets here:
# https://twiki.cern.ch/twiki/bin/viewauth/Atlas/FastTrackHLTintegration

pathena FTK2EVERYTHING.py \
    --nFilesPerJob 1 --supStream GLOBAL \
    --inDS valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.RDO_FTK.e3099_s2578_r7579_r7578 \
    --outDS user.end.110401.PowhegPythia_P2012_ttbar_nonallhad.FTK2EVERYTHING.e3099_s2578_r7579_r7578.v1
